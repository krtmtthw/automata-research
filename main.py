# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import nltk
from nltk.parse.generate import generate

import ioutils

def print_hi(name):
    # nltk.download('punkt')
    # nltk.download('averaged_perceptron_tagger')
    strings = ioutils.parse("res/source/natural_sentences_final.tsv")
    mismatched = ioutils.mismatched_strings(strings)
    eligible = ioutils.eligible_strings(strings)
    tokens = ioutils.generate_vocabulary(strings)


    tags = ioutils.tag(strings)
    # for nested_list in tags:
    #    print(nested_list)

    cfg_english = ioutils.create_production_rules(strings, 0)
    print(cfg_english)
    cfg_ilocano = ioutils.create_production_rules(strings, 2)
    # print(cfg_ilocano)
    # for sentence in generate(cfg_english, n=20):
    #    print(' '.join(sentence))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
