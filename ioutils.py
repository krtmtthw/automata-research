# The IO library
import csv, re, string
import json

import nltk, pycorenlp


def parse(filename="res/source/natural_sentences.tsv"):
    with open(filename) as f:
        return list(csv.reader(f, delimiter='\t'))


def mismatched_strings(strings):
    mismatched = []
    for line in strings:
        word_count_one = sum([i.strip(string.punctuation).isalpha() for i in line[1].split()])
        word_count_two = sum([i.strip(string.punctuation).isalpha() for i in line[2].split()])
        if word_count_one != word_count_two:
            mismatched.append(line)
    return mismatched


def eligible_strings(strings):
    eligible = []
    mismatched = mismatched_strings(strings)
    for line in strings:
        if line not in mismatched:
            eligible.append(line)
    return eligible


def generate_vocabulary(strings):
    english_words = []
    ilocano_words = []
    vocabulary = []
    for line in strings:
        english_words.extend(nltk.word_tokenize(line[0]))
        ilocano_words.extend(nltk.word_tokenize(line[1]))
    vocabulary.append(list(set(english_words)))
    vocabulary.append(list(set(ilocano_words)))
    return vocabulary


def tag_helper(line, index, row):
    pattern = []
    tokens = nltk.word_tokenize(line[index])
    tags = nltk.pos_tag(tokens)
    row.append(line[index])
    row.append([tag for (token, tag) in tags])
    return row


def tag(strings):
    tagged_sentences = []
    for line in strings:
        current_row = []
        tag_helper(line, 0, current_row)
        tag_helper(line, 2, current_row)
        tagged_sentences.append(current_row)
    return tagged_sentences


def generate_parse_tree(sentence, stanfordnlp):
    output = stanfordnlp.annotate(sentence, properties={
        'annotators': 'tokenize,ssplit,pos,depparse,parse',
        'outputFormat': 'json'
    })
    output = json.loads(output, strict=False)
    return output['sentences'][0]['parse']


def create_production_rules(strings, colindex):
    cfg = nltk.CFG.fromstring("""ROOT -> S
    """)
    # cfg.productions()

    stanfordnlp = pycorenlp.StanfordCoreNLP('http://localhost:9000')
    for line in strings:
         treeString = generate_parse_tree(line[colindex], stanfordnlp)
         tree = nltk.Tree.fromstring(treeString)
         for prod in tree.productions():
            if tree not in cfg.productions():
                 cfg.productions().append(prod)
    return cfg
